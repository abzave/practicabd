create procedure encontrarPeriodosVencidos
as begin
	select Usuario.idUsuario, Usuario.nombre, Usuario.apellido from Usuario 
	inner join Factura on Factura.idUsuario = Usuario.idUsuario
	inner join FacturaXProducto on FacturaXProducto.idFactura = Factura.idFactura
	inner join Inventario on Inventario.idProducto = FacturaXProducto.idProducto
	inner join Marca on Marca.idMarca = Inventario.idMarca
	inner join Periodo on Periodo.idPeriodo = Marca.idPeriodo
	where Factura.fecha + Periodo.duracion < GETDATE()
end

create procedure obtenerMontosCobrados
@fechaInicio as date = null,
@fechaFin as date = null,
@idSoporte as tinyint = null,
@nivelSoporte as tinyint = null,
@idCategoria as tinyint = null,
@categoria as varchar(50) = null,
@idCliente as int = null,
@nombreCliente as varchar(75) = null,
@mensajeError as varchar(75) = null out
as begin
	if @fechaInicio is null and @fechaFin is null and @idSoporte is null and @nivelSoporte is null
			and @idCategoria is null and @categoria is null and @idCliente is null 
			and @nombreCliente is null begin
		set @mensajeError = 'Debe ingresar al menos un parametro';
		raiserror(@mensajeError, 1, 1);
		return;
	end else if @fechaFin is not null and @fechaInicio is null or @fechaInicio is not null 
			and @fechaFin is null begin
		set @mensajeError = 'Debe ingresar ambas fechas';
		raiserror(@mensajeError, 1, 2);
		return;
	end else if @fechaInicio > GETDATE() or @fechaFin > GETDATE() begin
		set @mensajeError = 'Las fechas deben anteriores al d�a actual';
		raiserror(@mensajeError, 1, 3);
		return;
	end else if @fechaFin < @fechaInicio begin
		set @mensajeError = 'Las fecha final debe ser mayor a la final';
		raiserror(@mensajeError, 1, 3);
		return;
	end 
	if @nivelSoporte is not null or @idSoporte is not null begin
		if not exists (select idNivel = @idSoporte from Nivel where (@idSoporte is null 
				or idNivel = @idSoporte) and (@nivelSoporte is null or nivel = @nivelSoporte)) begin
			set @mensajeError = 'El nivel de soporte no existe';
			raiserror(@mensajeError, 2, 1);
			return;
		end
	end else if @categoria is not null or @idCategoria is not null begin
		if not exists (select idCategoria = @idCategoria from Categoria where (@idCategoria is null 
				or idCategoria = @idCategoria) and (@categoria is null or tipo = @categoria)) begin
			set @mensajeError = 'La categoria de soporte no existe';
			raiserror(@mensajeError, 2, 1);
			return;
		end
	end else if @nombreCliente is not null or @idCliente is not null begin
		if not exists (select idUsuario = @idCliente from Usuario where (@idCliente is null 
				or idUsuario = @idCliente) and (@nombreCliente is null or nombre = @nombreCliente)) begin
			set @mensajeError = 'El cliente de soporte no existe';
			raiserror(@mensajeError, 2, 1);
			return;
		end
	end
	select Factura.monto from Factura
	inner join Soporte on Soporte.idFactura = idFactura
	inner join Nivel on Nivel.idNivel = Soporte.idNivel
	inner join Categoria on Categoria.idCategoria = Soporte.idCategoria
	inner join Usuario on Usuario.idUsuario = Factura.idUsuario
	where (@fechaFin is null or @fechaInicio <= Factura.fecha and Factura.fecha <= @fechaFin)
	and (@idSoporte is null or Soporte.idNivel = @idSoporte) 
	and (@idCategoria is null or Soporte.idCategoria = @idCategoria)
	and (@idCliente is null or Factura.idUsuario = @idCliente)
end

create procedure clientEvaluation
@califacion as tinyint,
@comentario as varchar(75) = null,
@idUsuario as int,
@idSoporte as int,
@mensajeError as varchar(75) out = null,
@idEvaluacion as int out = null
as begin
	 if @califacion is null begin
		set @mensajeError = 'La calificacion no puede ser nula';
		raiserror(@mensajeError, 1, 1);
		return;
	 end else if @califacion < 0 begin
		set @mensajeError = 'La calificacion no puede ser negativa';
		raiserror(@mensajeError, 1, 4);
		return;
	 end else if @comentario = '' begin
		set @mensajeError = 'El comentario no puede estar vacio';
		raiserror(@mensajeError, 1, 5);
		return;
	 end else if @idUsuario is null begin
		set @mensajeError = 'El id del usuario no puede ser nulo';
		raiserror(@mensajeError, 1, 1);
		return;
	 end else if @idSoporte is null begin
		set @mensajeError = 'El id del soporte no puede ser nulo';
		raiserror(@mensajeError, 1, 1);
		return;
	 end else if @idUsuario < 0 begin
		set @mensajeError = 'El id del usuario no puede ser negativo';
		raiserror(@mensajeError, 1, 4);
		return;
	 end
	 begin transaction
	 insert into Evaluacion (calificacion, idSoporte, idUsuario) values (@califacion, @idSoporte, @idUsuario)
	 if @comentario is not null begin
		insert into Evaluacion (comentario) values (@comentario)
	 end
	 select @idEvaluacion = @@IDENTITY
	 commit transaction
end

create procedure obtenerCalifiacionServicio
@fechaInicio as date = null,
@fechaFin as date = null,
@nombreCliente as varchar(75) = null,
@idCliente as int = null,
@nivel as tinyint = null,
@idNivel as tinyint = null,
@mensajeError as varchar(75) = null
as begin
	if @fechaInicio is null and @fechaFin is null and @nombreCliente is null and @idCliente is null
			and @nivel is null and @idNivel is null begin
		set @mensajeError = 'Debe ingresar al menos un parametro';
		raiserror(@mensajeError, 1, 1);
		return;
	end else if @fechaFin is not null and @fechaInicio is null or @fechaInicio is not null 
			and @fechaFin is null begin
		set @mensajeError = 'Debe ingresar ambas fechas';
		raiserror(@mensajeError, 1, 2);
		return;
	end else if @fechaInicio > GETDATE() or @fechaFin > GETDATE() begin
		set @mensajeError = 'Las fechas deben anteriores al d�a actual';
		raiserror(@mensajeError, 1, 3);
		return;
	end else if @fechaFin < @fechaInicio begin
		set @mensajeError = 'Las fecha final debe ser mayor a la final';
		raiserror(@mensajeError, 1, 3);
		return;
	end 
	if @nivel is not null or @idNivel is not null begin
		if not exists (select idNivel = @idNivel from Nivel where (@idNivel is null 
				or idNivel = @idNivel) and (@nivel is null or nivel = @nivel)) begin
			set @mensajeError = 'El nivel de soporte no existe';
			raiserror(@mensajeError, 2, 1);
			return;
		end
	end else if @nombreCliente is not null or @idCliente is not null begin
		if not exists (select idUsuario = @idCliente from Usuario where (@idCliente is null 
				or idUsuario = @idCliente) and (@nombreCliente is null or nombre = @nombreCliente)) begin
			set @mensajeError = 'El cliente de soporte no existe';
			raiserror(@mensajeError, 2, 1);
			return;
		end
	end
	select Soporte.idSoporte, Evaluacion.califiacion, Evaluacion.comentario from Evaluacion
	inner join Soporte on Soporte.idSoporte = Evaluacion.idSoporte
	inner join Factura on Factura.idFactura = Soporte.idFactura
	inner join Nivel on Nivel.idNivel = Soporte.idNivel
	inner join Usuario on Usuario.idUsuario = Evaluacion.idUsuario
	where (@fechaFin is null or @fechaInicio <= Factura.fecha and Factura.fecha <= @fechaFin)
	and (@idNivel is null or Soporte.idNivel = @idNivel) 
	and (@idCliente is null or Evaluacion.idUsuario = @idCliente)
end